/++
 + Support utilities.
 +/
// SPDX-License-Identifier: Apache-2.0
module base.utils;

public
{
	/++
	 + Makes a string generation static to compile time.
	 +
	 + Macros:
	 +     GENERATED = Generated string to make static.
	 +/
	template staticString(string GENERATED)
	{
		enum staticString = GENERATED;
	}
	/++
	 + Generates an static formated string from template parameters.
	 +
	 + Macros:
	 +     FORMAT = Format string
	 +     VALUES = Values to insert into format string
	 +/
	template staticFormat(string FORMAT, VALUES...)
	{
		import std.format;
		enum staticFormat = format!FORMAT(VALUES);
	}

	/++
	 + Destroyes structs, arrays, ….
	 +
	 + Macros:
	 +     TYPE = Type of the value to destroy
	 +
	 + Params:
	 +     toDestroy = Reference to the value that sould be destroyed
	 +/
	pure nothrow @nogc @live void destroyValue(TYPE)(ref TYPE toDestroy)
	{
		import std.traits : isArray, isPointer;
		static if(!is(TYPE == void))
		{
			static if(is(TYPE == struct))
			{
				destroy(toDestroy);
			}
			else static if(isArray!TYPE)
			{
				foreach(ref i; toDestroy)
				{
					destroyValue(i);
				}
			}
			else static if(is(TYPE == class) || isPointer!(TYPE))
			{
				toDestroy = null;
			}
		}
	}
}

version(unittest)
{
	private
	{
		import base.asserts : assertEquals, TesterDeconstructor;
		// Test destroy value
		unittest
		{
			{
				TesterDeconstructor.Notified n;
				TesterDeconstructor td = TesterDeconstructor(&n);
				destroyValue(td);
				n.check();
			}
			{
				TesterDeconstructor.Notified n1, n2;
				TesterDeconstructor[] tmp = [TesterDeconstructor(&n1), TesterDeconstructor(&n2)];
				destroyValue(tmp);
				n1.check();
				n2.check();
			}
			{
				class Test{}
				Test tmp = new Test();
				destroyValue(tmp);
				assertEquals(tmp, null);
			}
		}
	}
}
