/++
 + Unittest utils to write better error messages and test cases.
 +/
// SPDX-License-Identifier: Apache-2.0
module base.asserts;

private
{
	import std.format;
	import std.traits;
	import core.exception;
	import std.algorithm.searching;

	string valuePrinter(TYPE)(TYPE value)
	{
		static if(is(TYPE == string))
		{
			// Get size
			char[] result;
			{
				size_t outputSize = 2;
				foreach(char i; value)
				{
					switch(i)
					{
						case '\\':
						case '\r':
						case '\n':
						case '\t':
						case '"':
							outputSize += 2;
							break;
						default:
							outputSize++;
							break;
					}
				}
				result = new char[outputSize];
			}

			// Generate output
			{
				size_t pos = 1;
				foreach (char i; value)
				{
					switch (i)
					{
						case '\\':
							result[pos] = '\\';
							result[pos + 1] = '\\';
							pos += 2;
							break;
						case '\r':
							result[pos] = '\\';
							result[pos + 1] = 'r';
							pos += 2;
							break;
						case '\n':
							result[pos] = '\\';
							result[pos + 1] = 'n';
							pos += 2;
							break;
						case '\t':
							result[pos] = '\\';
							result[pos + 1] = 't';
							pos += 2;
							break;
						case '"':
							result[pos] = '\\';
							result[pos + 1] = '"';
							pos += 2;
							break;
						default:
							result[pos] = i;
							pos++;
							break;
					}
				}
				result[0] = '"';
				result[$ - 1] = '"';
			}

			// Output
			return result.idup;
		}
		else
		{
			return format!"%s"(value);
		}
	}

	string printer(TYPE1, TYPE2)(ref TYPE1 value1, ref TYPE2 value2, string preprint)
	{
		static if(is(TYPE1 == string) && is(TYPE2 == string))
		{
			return format!"%s\n+ %s\n- %s"(preprint, valuePrinter(value1)[1..$-1], valuePrinter(value2)[1..$-1]);
		}
		else
		{
			return format!"%s %s <==> %s"(preprint, valuePrinter(value1), valuePrinter(value2));
		}
	}
}

public
{
	import std.exception;
	import core.exception : AssertError;

	/++
	 + Checks if two values are equal.
	 +
	 + Params:
	 +     value1 = Value one
	 +     value2 = Value two that should be equal
	 +/
	void assertEquals(TYPE1, TYPE2)(TYPE1 value1, TYPE2 value2)
	{
		if(value1 != value2)
		{
			assert(false, printer(value1, value2, "Expected equality, but got:"));
		}
	}
	/++
	 + Checks if two values are unequal.
	 +
	 + Params:
	 +     value1 = Value one
	 +     value2 = Value two that should be unequal
	 +/
	void assertUnequals(TYPE1, TYPE2)(TYPE1 value1, TYPE2 value2)
	{
		if(value1 == value2)
		{
			assert(false, printer(value1, value2, "Expected unequality, but got:"));
		}
	}

	/++
	 + Checks if error message is equal to the expected one.
	 +
	 + Params:
	 +     expression = Expression to generate the error
	 +     msg =        Expected message of the error
	 +/
	void assertExceptionMessageEquals(EXPR)(lazy EXPR expression, string msg)
	{
		bool thrown = false;
		try
		{
			expression;
		}
		catch(Throwable t)
		{
			if(t.msg != msg)
			{
				assert(false, printer(t.msg, msg, "Expected get error msg, but got:"));
			}
			thrown = true;
		}
		if(!thrown)
		{
			assert(false, "Expected that exception will be thrown.");
		}
	}

	/++
	 + Checks if two types are equal.
	 +
	 + Macros:
	 +     TYPE1 = Type one to compare
	 +     TYPE2 = Type two to compare
	 +/
	void assertSameType(TYPE1, TYPE2)()
	{
		bool result = is(TYPE1 == TYPE2);
		if (!result)
		{
			assert(result, format!"Types are unequal: %s <==> %s"(fullyQualifiedName!(TYPE1), fullyQualifiedName!(TYPE2)));
		}
	}

	/++
	 + Tester for deconstructor calls.
	 +/
	struct TesterDeconstructor
	{
		private
		{
			Notified* notifier;
		}
		public
		{
			/++
			 + Get notified when paren struct get deconstructed.
			 +/
			struct Notified
			{
				package
				{
					size_t internalNotifies = 0;
				}
				public
				{
					/++
					 + Getter for notifies counter.
					 +
					 + Returns: notifies
					 +/
					@property pure nothrow @nogc @live @safe size_t notifies() const
					{
						return this.internalNotifies;
					}

					/++
					 + Checks if it is notified exactly one time.
					 +/
					pure @live @safe void check() const
					{
						assert(this.notifies == 1, (
							this.notifies == 0 ?
							"Didn't cleaned up the content." :
							format!"Freed %s"(this.notifies)
						));
					}
				}
			}

			@disable this();
			/++
			 + Move constructor.
			 +
			 + Params:
			 +     td = Referenced tester to move
			 +/
			pure @nogc nothrow @live @safe this(ref TesterDeconstructor td)
			{
				this.notifier = td.notifier;
				td.notifier = null;
			}
			/++
			 + Constructor for with callback notified reference.
			 +
			 + Params:
			 +     notifier = Target to store the deconstruction callback.
			 +/
			pure @nogc nothrow @live @safe this(Notified* notifier)
			{
				this.notifier = notifier;
			}
			/++
			 + Deconstructor callback to update.
			 +/
			pure @nogc nothrow @live @safe ~this()
			{
				if(this.notifier !is null)
				{
					this.notifier.internalNotifies++;
				}
			}

			/++
			 + Debug printing support.
			 +
			 + Returns: debug string
			 +/
			@safe pure string toString() const
			{
				if(this.notifier is null)
				{
					return "TesterDeconstructor(null)";
				}
				else
				{
					return format!"TesterDeconstructor(notified: %s)"(this.notifier.notifies);
				}
			}
		}
	}
}

private
{
	// Value printer
	unittest
	{
		assertEquals(valuePrinter("\\\"\t\r\n"), "\"\\\\\\\"\\t\\r\\n\"");
	}

	// Assert equals
	unittest
	{
		assertEquals(0, 0);
		assertThrown!(AssertError)(assertEquals(0, 1));
		try
		{
			assertEquals(0, 1);
		}
		catch (AssertError ae)
		{
			assert(ae.msg == "Expected equality, but got: 0 <==> 1");
		}
	}

	// Assert unequals
	unittest
	{
		assertUnequals(0, 1);
		assertThrown!(AssertError)(assertUnequals(0, 0));
		try
		{
			assertUnequals(0, 0);
		}
		catch(AssertError ae)
		{
			assert(ae.msg == "Expected unequality, but got: 0 <==> 0");
		}
	}

	// Assert exception message same
	unittest
	{
		assertExceptionMessageEquals(assert(false, "ABC"), "ABC");
		assertThrown!(AssertError)(assertExceptionMessageEquals(assert(false), ""));
		try
		{
			assertExceptionMessageEquals(1 + 1, "");
		}
		catch(AssertError ae)
		{
			assertEquals(ae.msg, "Expected that exception will be thrown.");
		}
		try
		{
			assertExceptionMessageEquals(assert(false, "ABC"), "DEF");
		}
		catch(AssertError ae)
		{
			assert(ae.msg.startsWith("Expected get error msg, but got:"));
		}
	}

	// Assert same type
	unittest
	{
		assertSameType!(int, int)();
		assertThrown!AssertError(assertSameType!(int, uint)());
		try
		{
			assertSameType!(int, uint)();
		}
		catch (AssertError ae)
		{
			assert(ae.msg == "Types are unequal: int <==> uint", ae.msg);
		}
	}

	// Tester deconstructor calls
	pure @live unittest
	{
		TesterDeconstructor.Notified n1;
		TesterDeconstructor.Notified n2;
		TesterDeconstructor.Notified n3;

		{
			TesterDeconstructor t2_1 = TesterDeconstructor(&n2);
			TesterDeconstructor t2_2 = t2_1;
			assertEquals(format!"%s"(t2_1), "TesterDeconstructor(null)");
			assertEquals(format!"%s"(t2_2), "TesterDeconstructor(notified: 0)");
			TesterDeconstructor t3_1 = TesterDeconstructor(&n3);
			TesterDeconstructor t3_2 = TesterDeconstructor(&n3);
		}

		assertThrown!AssertError(n1.check());
		assertExceptionMessageEquals(n1.check(), "Didn't cleaned up the content.");
		n2.check();
		assertThrown!AssertError(n3.check());
		assertExceptionMessageEquals(n3.check(), "Freed 2");
	}
}
