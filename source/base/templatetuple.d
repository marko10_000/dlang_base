/++
 + Operrations on the template tuples.
 +/
// SPDX-License-Identifier: Apache-2.0
module base.templatetuple;

private
{
	template _TEnumerate(int ID, T...)
	{
		static if(T.length == 0)
		{
			alias _TEnumerate = TemplateTuple!();
		}
		else
		{
			alias _TEnumerate = TemplateTuple!(TemplateContainer!(ID, T[0]), _TEnumerate!(ID + 1, T[1..$]));
		}
	}
}

public
{
	/++
	 + A template alias that makes the idenetity.
	 +
	 + Macros:
	 +     ENTRY = Entry to output
	 +/
	template TemplateIdent(alias ENTRY)
	{
		alias TemplateIdent = ENTRY;
	}
	/++
	 + Generates a template tuple.
	 +
	 + Macros:
	 +     ENTRIES = Entries of the template tuple
	 +/
	template TemplateTuple(ENTRIES...)
	{
		alias TemplateTuple = ENTRIES;
	}
	/++
	 + Container type to contain template tuple entries.
	 +
	 + Macros:
	 +     ENTRIES = Entries of the content
	 +/
	struct TemplateContainer(ENTRIES...)
	{
		alias CONTENT = ENTRIES; /// Entries of the the template tuple
	}
	/++
	 + Filters a template tuple.
	 +
	 + Macros:
	 +     FILTER =  Filter template which get insert with the type to check
	 +     ENTRIES = Entries of the template tuple to filter
	 +/
	template TemplateTupleFilter(alias FILTER, ENTRIES...)
	{
		static if(ENTRIES.length == 0)
		{
			alias TemplateTupleFilter = TemplateTuple!();
		}
		else static if(FILTER!(ENTRIES[0]))
		{
			alias TemplateTupleFilter = TemplateTuple!(ENTRIES[0], TemplateTupleFilter!(FILTER, ENTRIES[1..$]));
		}
		else
		{
			alias TemplateTupleFilter = TemplateTupleFilter!(FILTER, ENTRIES[1..$]);
		}
	}
	/++
	 + Mapping template tuple.
	 +
	 + Macros:
	 +     MAPPER =  Mapper template which will be insert with to map
	 +     ENTRIES = Template tuple to map
	 +/
	template TemplateTupleMap(alias MAPPER, ENTRIES...)
	{
		static if(ENTRIES.length == 0)
		{
			alias TemplateTupleMap = TemplateTuple!();
		}
		else
		{
			alias TemplateTupleMap = TemplateTuple!(MAPPER!(ENTRIES[0]), TemplateTupleMap!(MAPPER, ENTRIES[1..$]));
		}
	}
	/++
	 + Enumerrate template tuple. Starting with 0.
	 +
	 + Macros:
	 +     ENTRIES = Entries that should be enumerated
	 +/
	template TemplateTupleEnumerate(ENTRIES...)
	{
		alias TemplateTupleEnumerate = _TEnumerate!(0, ENTRIES);
	}
	/++
	 + Zips containers together. Will stop when the shortest is at the end.
	 +
	 + Macros:
	 +     CONTAINER1 = First container
	 +     CONTAINER2 = Second container
	 +/
	template TemplateTupleZip(CONTAINER1, CONTAINER2)
	{
		static if(CONTAINER1.CONTENT.length > 0 && CONTAINER2.CONTENT.length > 0)
		{
			alias TemplateTupleZip = TemplateTuple!(
				TemplateContainer!(CONTAINER1.CONTENT[0], CONTAINER2.CONTENT[0]),
				TemplateTupleZip!(
					TemplateContainer!(CONTAINER1.CONTENT[1..$]),
					TemplateContainer!(CONTAINER2.CONTENT[1..$])
				)
			);
		}
		else
		{
			alias TemplateTupleZip = TemplateTuple!();
		}
	}
}

version(unittest)
{
	private
	{
		import base.asserts : assertSameType;

		// Test container
		unittest
		{
			assertSameType!(
					TemplateContainer!(TemplateTuple!(int, float)),
					TemplateContainer!(TemplateTuple!(int, TemplateTuple!(float)))
			)();
		}

		// Test filter
		unittest
		{
			assertSameType!(
				TemplateContainer!(TemplateTupleFilter!(TemplateIdent, 0, 1, 0, 2, true)),
				TemplateContainer!(TemplateTuple!(1, 2, true))
			)();
		}

		// Test mapper
		unittest
		{
			template Add1(alias A)
			{
				enum Add1 = A + 1;
			}

			assertSameType!(
					TemplateContainer!(TemplateTupleMap!(Add1, -1, 0, 1, 2)),
					TemplateContainer!(0, 1, 2, 3)
			)();
		}

		// Test enumerate
		unittest
		{
			assertSameType!(
				TemplateContainer!(TemplateTupleEnumerate!(int, float, uint)),
				TemplateContainer!(TemplateContainer!(0, int), TemplateContainer!(1, float), TemplateContainer!(2, uint))
			);
		}

		// Test zip
		unittest
		{
			assertSameType!(
					TemplateContainer!((TemplateTupleZip!(TemplateContainer!(int, float), TemplateContainer!(uint, double, size_t)))),
					TemplateContainer!(TemplateContainer!(int, uint), TemplateContainer!(float, double))
			);
			assertSameType!(
				TemplateContainer!((TemplateTupleZip!(TemplateContainer!(int, float, size_t), TemplateContainer!(uint, double)))),
				TemplateContainer!(TemplateContainer!(int, uint), TemplateContainer!(float, double))
			);
		}
	}
}
