/++
 + Allow to box a type to store it on the heap.
 +/
// SPDX-License-Identifier: Apache-2.0
module base.containers.box;

private
{
	import base.templatetuple : TemplateContainer, TemplateTuple;

	enum MallocType
	{
		Done,
		GC
	}

	union BoxValue(TYPE)
	{
		TYPE value;
	}

	struct BoxGenerator(TYPE, MallocType MT)
	{
		package
		{
			nothrow pure @nogc @safe void function(BoxValue!TYPE* boxValue) deallocator;
			static if(MT == MallocType.Done)
			{
				BoxValue!TYPE* boxValue;
			}
			else
			{
				TYPE boxValue;
			}
		}
	}
}

public
{
	/++
	 + Boxes a type onto the stack to solve recursive structs. Boxes have to be always initalized.
	 +
	 + Macros:
	 +     TYPE = Struct to box
	 +/
	struct Box(TYPE)
	{
		static assert(is(TYPE == struct), "Only structs can be boxed.");
		private
		{
			nothrow pure @nogc @safe void function(BoxValue!TYPE* boxValue) __deallocator;
			BoxValue!TYPE* __boxValue;
		}
		public
		{
			/++
			 + Access to the stored box value.
			 +
			 + Returns: box value
			 +/
			@property pure nothrow @live @nogc @trusted ref TYPE boxValue()
			{
				if(this.__boxValue == null)
				{
					assert(0, "Box isn't initalized.");
				}
				else
				{
					return this.__boxValue.value;
				}
			}
			@property pure nothrow @live @nogc @trusted ref const(TYPE) boxValue() const
			{
				if(this.__boxValue == null)
				{
					assert(0, "Box isn't initalized.");
				}
				else
				{
					return this.__boxValue.value;
				}
			}

			/++
			 + Write to the stored box value.
			 +
			 + Params:
			 +     value = Value to write
			 +
			 + Returns: Reference to the boxed value
			 +/
			@property pure nothrow @live @nogc @trusted ref TYPE boxValue(return TYPE value) return
			{
				if(this.__boxValue == null)
				{
					assert(0, "Box isn't initalized.");
				}
				else
				{
					return this.__boxValue.value = value;
				}
			}

			/++
			 + Constractor to allocate the box by d's gc.
			 +
			 + Macros:
			 +     GEN_TYPE = Type of the stored value
			 +     MT =       Allocated type that have to be GC
			 +
			 + Params:
			 +     value = Boxed value to store
			 +/
			nothrow pure @live @safe this(GEN_TYPE, MallocType MT)(return BoxGenerator!(GEN_TYPE, MT) value) if (
				__traits(compiles, {GEN_TYPE* a; TYPE b = *a;} && MT == MallocType.GC)
			)
			{
				static nothrow pure @nogc @safe void deallocate(BoxValue!TYPE* value) {}

				this.__deallocator = &deallocate;
				static if(__traits(isSame, GEN_TYPE, TYPE))
				{
					this.__boxValue = value.boxValue;
				}
				else
				{
					this.__boxValue = new BoxValue!TYPE;
					this.__boxValue.value = TYPE(value.boxValue);
				}
			}
			/++
			 + Constructor to use allready allocated box.
			 +
			 + Params:
			 +     value = Value to
			 +/
			nothrow pure @live @nogc @safe this(MallocType MT)(return BoxGenerator!(TYPE, MT) value) if (
				__traits(compiles, {GEN_TYPE* a; TYPE b = TYPE(a.entries);} && MT == MallocType.Done)
			)
			{
				this.__deallocator = value.deallocator;
				this.__boxValue = value.boxValue;
			}
			@disable this();

			/++
			 + Cleanup and deallocat data.
			 +/
			nothrow pure @live @nogc @safe ~this()
			{
				if(this.__boxValue !is null)
				{
					this.__deallocator(this.__boxValue);
				}
			}
			alias boxValue this;

			/++
			 + Update box value.
			 +
			 + Params:
			 +     value = Value to set.
			 +
			 + Returns: Reference to the updated type.
			 +/
			nothrow pure @live @nogc @safe ref TYPE opAssign(return TYPE value)
			{
				return this.boxValue = value;
			}
			/++
			 + Update box value by destroying old value and generate new one by d's gc.
			 +
			 + Macros:
			 +     GEN_TYPE = Type to store/init the value
			 +     MT       = Kind of box value generation. Here it has to be GC
			 +
			 + Params:
			 +     value = Value to store
			 +
			 + Returns: Reference to the updated boxed value.
			 +/
			nothrow pure @live @safe ref TYPE opAssign(GEN_TYPE, MallocType MT)(return BoxGenerator!(GEN_TYPE, MT) value) if (
				__traits(compiles, {GEN_TYPE* a; TYPE b = TYPE(*a);}) && MT == MallocType.GC
			)
			{
				static nothrow pure @nogc @safe void deallocate(BoxValue!TYPE* value) {}

				if(this.__boxValue !is null)
				{
					this.__deallocator(this.__boxValue);
					this.__boxValue = null;
					this.__deallocator = null;
				}
				this.__deallocator = &deallocate;
				this.__boxValue = new BoxValue!TYPE;
				static if(__traits(isSame, GEN_TYPE, TYPE))
				{
					this.__boxValue.value = value.boxValue;
				}
				else
				{
					this.__boxValue.value = TYPE(value.boxValue);
				}
				return this.boxValue;
			}

			/++
			 + Helper to generate a box by d's gc allocator.
			 +
			 + Params:
			 +     values = Values to insert into the constructor
			 +
			 + Returns: Box generator
			 +/
			static nothrow pure @live @safe Box!TYPE generateBoxGC(TYPES...)(return TYPES values) if(
				__traits(compiles, {new TYPE(values);})
			)
			{
				static nothrow pure @nogc @safe void deallocate(BoxValue!TYPE* value) {}

				BoxValue!TYPE* tmp = new BoxValue!TYPE;
				tmp.value = TYPE(values);
				BoxGenerator!(TYPE, MallocType.Done) result = {boxValue: tmp, deallocator: &deallocate};
				Box!TYPE result2 = result;
				return result2;
			}
		}
	}

	/++
	 + Generates a box generator by a single parameter.
	 +
	 + Macros:
	 +     TYPE = Type to store for allocation
	 +
	 + Params:
	 +   value = Value to store
	 +
	 + Returns: Generator for the box
	 +/
	nothrow pure @live @safe BoxGenerator!(TYPE, MallocType.GC) boxGC(TYPE)(return TYPE value)
	{
		BoxGenerator!(TYPE, MallocType.GC) result = {boxValue: value, deallocator: null};
		return result;
	}
}

version(unittest)
{
	private
	{
		import base.asserts;

		@live unittest
		{
			struct Test
			{
				int a;
			}
			Box!Test tmp = Box!Test.generateBoxGC();
			tmp.a = 1;
			assertEquals(tmp.a, 1);
			{
				Test tmp2 = {a: 2};
				tmp = tmp2;
			}
			assertEquals(tmp.a, 2);
			tmp.a = 3;
			assertEquals((cast(const(Box!Test)) tmp).a, 3);
			tmp = Box!Test.generateBoxGC(4);
			assertEquals(tmp.a, 4);
			tmp = boxGC(5);
			assertEquals(tmp.a, 5);
		}
	}
}
