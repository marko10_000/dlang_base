/++
 + Optinal like in rust.
 +/
// SPDX-License-Identifier: Apache-2.0
module base.containers.optional;

public
{
	/++
	 + An safe optional result. [void] is not supported.
	 +
	 + Macros:
	 +     TYPE = Type of value to store
	 +/
	struct Optional(TYPE)
	{
		static assert(!is(TYPE == void), "Void isn't a valid type. You could use the Result type as an alternative.");
		private
		{
			import base.utils : destroyValue;

			bool internalIsSet = false;
			union Ops
			{
				char generated;
				TYPE value;
			}
			Ops internalOps = {generated: 0xEE};

			pure @nogc nothrow @live @trusted void internalDestroy()
			{
				if(this.internalIsSet)
				{
					destroyValue(this.internalOps.value);
					this.internalIsSet = false;
				}
			}
		}
		public
		{
			/++
			 + Checks if it's set.
			 +
			 + Returns: true when set
			 +/
			@property pure @nogc nothrow @live @safe bool isSet() const
			{
				return this.internalIsSet;
			}
			/++
			 + Checks if it's empty.
			 +
			 + Returns: true when it's empty
			 +/
			@property pure @nogc nothrow @live @safe bool isEmpty() const
			{
				return !this.internalIsSet;
			}

			/++
			 + Allow acces to the variable. Asserts when not set.
			 +
			 + Returns: referenc to the value
			 +/
			@property pure @nogc nothrow @live @trusted ref TYPE value()
			{
				if (this.internalIsSet)
				{
					return this.internalOps.value;
				}
				else
				{
					assert(0);
				}
			}
			@property pure @nogc nothrow @live @trusted ref const(TYPE) value() const
			{
				if (this.internalIsSet)
				{
					return this.internalOps.value;
				}
				else
				{
					assert(0);
				}
			}
			/++
			 + Allow to set value.
			 +
			 + Params:
			 +     value = Value to set
			 +
			 + Returns: Reference to the set value
			 +/
			@property pure @nogc nothrow @live @trusted ref TYPE value(return TYPE value)
			{
				this.internalDestroy();
				this.internalIsSet = true;
				Ops tmp = {value: value};
				this.internalOps = tmp;
				return this.internalOps.value;
			}

			/++
			 + Move constructor.
			 +
			 + Params:
			 +     toMove = Value to move
			 +/
			pure @nogc nothrow @live @trusted this(return TYPE toMove)
			{
				this.internalIsSet = false;
				this.internalOps.generated = 0xF7;
				this.value = toMove;
			}
			/++
			 + Set value to init.
			 +
			 + Params:
			 +     toMove = Inital value to set.
			 +/
			pure @nogc nothrow @live @trusted this(return ref Optional!TYPE toMove)
			{
				this.internalIsSet = false;
				this.internalOps.generated = 0xF8;
				if(toMove.internalIsSet)
				{
					this.value = toMove.value;
				}
			}
			/++
			 + Cleanup data.
			 +/
			pure @nogc nothrow @live @trusted ~this()
			{
				this.internalDestroy();
			}
		}
	}
}

version(unittest)
{
	import base.asserts : TesterDeconstructor, assertEquals, assertThrown, AssertError;
	unittest
	{
		TesterDeconstructor.Notified n;
		{
			Optional!TesterDeconstructor t = TesterDeconstructor(&n);
			assertEquals(t.isSet, true);
			assertEquals(t.isEmpty, false);
		}
		n.check();
	}
	unittest
	{
		Optional!size_t t;
		assertEquals(t.isEmpty, true);
		assertEquals(t.isSet, false);
		assertThrown!(AssertError)(t.value);
		t = Optional!size_t(1);
		assertEquals(t.isEmpty, false);
		assertEquals(t.isSet, true);
		assertEquals(t.value, 1);
		const(Optional!size_t) t2;
		assertEquals(t2.isEmpty, true);
		assertEquals(t2.isSet, false);
		assertThrown!(AssertError)(t2.value);
		const(Optional!size_t) t3 = t;
		assertEquals(t3.isEmpty, false);
		assertEquals(t3.isSet, true);
		assertEquals(t3.value, 1);
	}
	unittest
	{
		Optional!size_t t;
		t.value = 1;
		assertEquals(t.isSet, true);
		assertEquals(t.isEmpty, false);
		assertEquals(t.value, 1);
	}
	unittest
	{
		TesterDeconstructor.Notified n;
		{
			Optional!TesterDeconstructor t1 = Optional!TesterDeconstructor(TesterDeconstructor(&n));
			Optional!TesterDeconstructor t2 = t1;
		}
		n.check();
	}
	unittest
	{
		Optional!TesterDeconstructor t1;
		assertEquals(t1.isEmpty, true);
		Optional!TesterDeconstructor t2 = t1;
		assertEquals(t2.isEmpty, true);
	}
}
