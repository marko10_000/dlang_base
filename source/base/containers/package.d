/++
 + Base containers.
 +/
// SPDX-License-Identifier: Apache-2.0
module base.containers;

public
{
	import base.containers.box : Box, boxGC;
	import base.containers.optional : Optional;
	import base.containers.result : Result, Ok, Err;
	import base.containers.tagedunion : TaggedUnionedValues, TaggedUnion;
}
