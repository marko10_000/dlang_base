/++
 + Add result type known from rust.
 +/
// SPDX-License-Identifier: Apache-2.0
module base.containers.result;

private
{
	struct ContainerOk(T)
	{
		package
		{
			import std.traits : TemplateOf;

			alias TYPE = T;
			static if(!is(T == void))
			{
				T value;

				pure @nogc nothrow @live @safe this(return T t)
				{
					this.value = t;
				}
			}
		}
		public
		{
			pure @nogc nothrow @live @safe RESULT opCast(RESULT)() if(
				__traits(isSame, TemplateOf!RESULT, Result) &&
				__traits(compiles, {ContainerOk!T co; RESULT r = RESULT(co);})
			)
			{
				return RESULT(this);
			}
		}
	}
	struct ContainerErr(T)
	{
		package
		{
			import std.traits : TemplateOf;

			alias TYPE = T;
			static if(!is(T == void))
			{
				T value;

				pure @nogc nothrow @live @safe this(return T t)
				{
					this.value = t;
				}
			}
		}
		public
		{
			pure @nogc nothrow @live @safe RESULT opCast(RESULT)() if(
				__traits(isSame, TemplateOf!RESULT, Result) &&
				__traits(compiles, {ContainerErr!T co; RESULT r = RESULT(co);})
			)
			{
				return RESULT(this);
			}
		}
	}
}

public
{
	/++
	 + Result type for an operation.
	 +
	 + Macros:
	 +     OK =  Value type when successfull
	 +     ERR = Value type when not successfull
	 +/
	struct Result(OK, ERR)
	{
		private
		{
			import std.format : format;
			import std.traits : TemplateOf, isPointer;
			import base.utils : destroyValue;

			bool internIsOk;
			union
			{
				static if(!is(OK == void))
				{
					OK internOk;
				}
				static if(!is(ERR == void))
				{
					ERR internErr;
				}
			}
		}
		public
		{
			/++
			 + Getter for if operation was successfull.
			 +
			 + Returns: is okey
			 +/
			@property pure @nogc nothrow @live @safe bool isOk() const
			{
				return this.internIsOk;
			}

			/++
			 + Getter for if operation wasn't successfull.
			 +
			 + Returns: is error
			 +/
			@property pure @nogc nothrow @live @safe bool isErr() const
			{
				return !this.internIsOk;
			}
			static if(!is(OK == void))
			{
				/++
				 + Getter for then successfull result when it's not void.
				 +
				 + Returns: result value
				 +/
				@property pure @nogc nothrow @live @trusted ref const(OK) ok() return const
				{
					if(isOk)
					{
						return this.internOk;
					}
					else
					{
						assert(0, "Value isn't ok.");
					}
				}
			}
			static if(!is(ERR == void))
			{
				/++
				 + Getter for the error value when it's not null
				 +
				 + Returns: error value
				 +/
				@property pure @nogc nothrow @live @trusted ref const(ERR) err() return const
				{
					if(isErr)
					{
						return this.internErr;
					}
					else
					{
						assert(0, "Value isn't err.");
					}
				}
			}

			@disable this();

			/++
			 + Initalize with an okey value.
			 +
			 + Macros:
			 +     TYPE = Value type of the content
			 +
			 + Params:
			 +   ok = Okey value to set
			 +/
			pure @nogc nothrow @live @trusted this(TYPE)(TYPE ok) if(
				__traits(isSame, TemplateOf!TYPE, ContainerOk) &&
				(__traits(compiles, {TYPE t; OK o = t.value;}) || is(OK == TYPE.TYPE))
			)
			{
				static if(!is(OK == void))
				{
					this.internOk = ok.value;
				}
				this.internIsOk = true;
			}
			/++
			 + Initalize with an error value.
			 +
			 + Macros:
			 +     TYPE = Value type of the content
			 +
			 + Params:
			 +     err = Value to save
			 +/
			pure @nogc nothrow @live @trusted this(TYPE)(TYPE err) if(
				__traits(isSame, TemplateOf!TYPE, ContainerErr) &&
				(__traits(compiles, {TYPE t; ERR e = t.value;}) || is(ERR == TYPE.TYPE))
			)
			{
				static if(!is(ERR == void))
				{
					this.internErr = err.value;
				}
				this.internIsOk = false;
			}

			/++
			 + Deconstructor to free/deconstruct content.
			 +/
			pure @nogc nothrow @live @trusted ~this() {
				if(this.internIsOk)
				{
					static if(!is(OK == void))
					{
						destroyValue(this.internOk);
					}
				}
				else
				{
					static if(!is(ERR == void))
					{
						destroyValue(this.internErr);
					}
				}
			}

			/++
			 + Generates content string when value should be printed.
			 +
			 + Returns: content value
			 +/
			@trusted pure string toString() const
			{
				if(this.isOk)
				{
					static if(is(OK == void))
					{
						return "Ok()";
					}
					else
					{
						return format!"Ok(%s)"(cast(OK) this.ok);
					}
				}
				else
				{
					static if(is(ERR == void))
					{
						return "Err()";
					}
					else
					{
						return format!"Err(%s)"(cast(ERR) this.err);
					}
				}
			}
		}
	}

	/++
	 + Generates an empty okey result.
	 +
	 + Returns: empty okey result
	 +/
	pure @nogc nothrow @live @safe ContainerOk!void Ok()
	{
		ContainerOk!void result;
		return result;
	}
	/++
	 + Generates an okey result.
	 +
	 + Params:
	 +     value = Value to store
	 +
	 + Returns: okey result
	 +/
	pure @live ContainerOk!TYPE Ok(TYPE)(return TYPE value)
	{
		return ContainerOk!(TYPE)(value);
	}

	/++
	 + Generates an empty error result.
	 +
	 + Returns: empty error result
	 +/
	pure @nogc nothrow @live @safe ContainerErr!void Err()
	{
		ContainerErr!void result;
		return result;
	}
	/++
	 + Generates an error result.
	 +
	 + Params:
	 +     value = Value to store
	 +
	 + Returns: error result
	 +/
	pure @live ContainerErr!TYPE Err(TYPE)(return TYPE value)
	{
		return ContainerErr!(TYPE)(value);
	}
}

version(unittest)
{
	import base.asserts;
	import std.format : format;

	private
	{
		// Test void, void
		@safe @live pure unittest
		{
			Result!(void, void) rv1 = Ok();
			assertEquals(rv1.isOk, true);
			assertEquals(rv1.isErr, false);
			assert(!__traits(compiles, {rv1.ok;}));
			assert(!__traits(compiles, {rv1.err;}));
			rv1 = cast(Result!(void, void)) Err();
			assertEquals(rv1.isOk, false);
			assertEquals(rv1.isErr, true);
		}

		// Test int, void
		@live pure unittest
		{
			Result!(int, void) rv1 = Ok(10);
			assertEquals(rv1.isOk, true);
			assertEquals(rv1.isErr, false);
			assertEquals(rv1.ok, 10);
			assert(!__traits(compiles, { rv1.err; }));
			rv1 = cast(Result!(int, void)) Err();
			assertEquals(rv1.isOk, false);
			assertEquals(rv1.isErr, true);
			assertThrown!AssertError(rv1.ok);
		}

		// Test void, int
		@live pure unittest
		{
			Result!(void, int) rv1 = Ok();
			assertEquals(rv1.isOk, true);
			assertEquals(rv1.isErr, false);
			assert(!__traits(compiles, { rv1.ok; }));
			assertThrown!AssertError(rv1.err);
			rv1 = cast(Result!(void, int)) Err(11);
			assertEquals(rv1.isOk, false);
			assertEquals(rv1.isErr, true);
			assertEquals(rv1.err, 11);
		}

		// Test int, int
		@live pure unittest
		{
			Result!(int, int) rv1 = Ok(12);
			assertEquals(rv1.isOk, true);
			assertEquals(rv1.isErr, false);
			assertEquals(rv1.ok, 12);
			assertThrown!AssertError(rv1.err);
			rv1 = cast(Result!(int, int)) Err(13);
			assertEquals(rv1.isOk, false);
			assertEquals(rv1.isErr, true);
			assertThrown!AssertError(rv1.ok);
			assertEquals(rv1.err, 13);
		}

		// Compiletest
		@safe @nogc pure @live nothrow unittest
		{
			Result!(int, int) rv1 = Ok(14);
			Result!(int, int) rv2 = Err(15);
			assert(rv1.isOk);
			assert(!rv1.isErr);
			assert(rv1.ok == 14);
			assert(!rv2.isOk);
			assert(rv2.isErr);
			assert(rv2.err == 15);
		}

		// Test string output
		unittest
		{
			assertEquals(format!"%s"(Result!(void, int)(Ok())), "Ok()");
			assertEquals(format!"%s"(Result!(void, int)(Err(10))), "Err(10)");
			assertEquals(format!"%s"(Result!(int, void)(Ok(11))), "Ok(11)");
			assertEquals(format!"%s"(Result!(int, void)(Err())), "Err()");
		}

		// Free test
		pure unittest
		{
			alias Type = Result!(TesterDeconstructor, TesterDeconstructor);
			{
				TesterDeconstructor.Notified n;
				{
					Type ty = Ok(TesterDeconstructor(&n));
					assertThrown!AssertError(n.check());
				}
				n.check();
			}
			{
				TesterDeconstructor.Notified n;
				{
					Type ty = Err(TesterDeconstructor(&n));
					assertThrown!AssertError(n.check());
				}
				n.check();
			}
			{
				TesterDeconstructor.Notified n1;
				TesterDeconstructor.Notified n2;
				{
					Type ty = Ok(TesterDeconstructor(&n1));
					assertThrown!AssertError(n1.check());
					assertThrown!AssertError(n2.check());
					ty = cast(Type) Err(&n2);
					n1.check();
					assertThrown!AssertError(n2.check());
				}
				n1.check();
				n2.check();
			}
		}
	}
}
