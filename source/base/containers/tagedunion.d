/++
 + Add support for tagged unions.
 +/
// SPDX-License-Identifier: Apache-2.0
module base.containers.tagedunion;

private
{
	// Will not be come public because it isn't safe code
	struct UnionedTypes(VALUES...)
	{
		private
		{
			import base.utils : staticFormat;
			import base.templatetuple : TemplateTupleEnumerate;
			import std.traits : ConstOf;

			union Vs
			{
				char generated;
				static foreach(T; TemplateTupleEnumerate!(VALUES))
				{
					static if(!is(T.CONTENT[1] == void))
					{
						mixin(staticFormat!("VALUES[%s] v%s;", T.CONTENT[0], T.CONTENT[0]));
					}
				}
			}
			Vs vs = {generated: 0};
		}
		public
		{
			pure nothrow @nogc @live ref VALUES[ID] get(size_t ID)() return if (
				0 <= ID && ID < VALUES.length && !is(VALUES[ID] == void)
			)
			{
				return __traits(getMember, this.vs, staticFormat!("v%s", ID));
			}
			pure nothrow @nogc @live ref ConstOf!(VALUES[ID]) get(size_t ID)() const return if (
				0 <= ID && ID < VALUES.length && !is(VALUES[ID] == void)
			)
			{
				return __traits(getMember, this.vs, staticFormat!("v%s", ID));
			}


			pure nothrow @nogc @live ref VALUES[ID] set(size_t ID)(return ref VALUES[ID] value) return if (
				0 <= ID && ID < VALUES.length && !is(VALUES[ID] == void)
			)
			{
				mixin(staticFormat!("Vs v = {v%s: value};", ID));
				this.vs = v;
				return __traits(getMember, this.vs, staticFormat!("v%s", ID));
			}
		}
	}
}

public
{
	/++
	 + Safe union of types (void is supported). Be aware it can be empty.
	 +
	 + Macros:
	 +     VALUES = Types inside of the union
	 +/
	struct TaggedUnionedValues(VALUES...)
	{
		private
		{
			import base.templatetuple : TemplateTupleEnumerate;
			import base.utils : destroyValue;
			import std.traits : isPointer;

			size_t internalTag = 0;
			UnionedTypes!(VALUES) internalValues = {};

			pure nothrow @nogc @live @trusted void internalDestroy()
			{
				slabel: switch(this.tag)
				{
					case 0:
						break;
					static foreach(T; TemplateTupleEnumerate!(VALUES))
					{
						case T.CONTENT[0] + 1:
							static if(!is(T.CONTENT[1] == void))
							{
								destroyValue(this.internalValues.get!(T.CONTENT[0]));
							}
							break slabel;
					}
					default:
						assert(0);
				}
			}
		}
		public
		{
			/++
			 + Returns the tag of the union. 0 when empty.
			 +
			 + Returns: tag of the union
			 +/
			pure nothrow @nogc @live @safe @property size_t tag() const
			{
				return this.internalTag;
			}
			/++
			 + Returns if union is empty.
			 +
			 + Returns: is empty
			 +/
			pure nothrow @nogc @live @safe @property bool isEmpty() const
			{
				return this.internalTag == 0;
			}

			/++
			 + Checks the tag and returns the value of then union. Could throw an exception.
			 +
			 + Macros:
			 +     ID = Entry number to access. Counting from 1,2,…
			 +
			 + Returns: value of the entry
			 +/
			pure nothrow @nogc @live @trusted @property ref VALUES[ID - 1] value(size_t ID)() return if(
				1 <= ID && ID <= VALUES.length && !is(VALUES[ID - 1] == void)
			)
			{
				if(this.internalTag == ID)
				{
					return this.internalValues.get!(ID - 1);
				}
				else
				{
					assert(0);
				}
			}
			/++
			 + Store new value.
			 +
			 + Macros:
			 +     ID = Entry number to access. Counting from 1,2,…
			 +
			 + Params:
			 +   toStore = Value to store
			 +
			 + Returns: reference to the store value
			 +/
			pure nothrow @nogc @live @trusted @property ref VALUES[ID - 1] value(size_t ID)(return VALUES[ID - 1] toStore)
				return if(1 <= ID && ID <= VALUES.length && !is(VALUES[ID - 1] == void))
			{
				this.internalDestroy();
				this.internalValues.set!(ID - 1)(toStore);
				this.internalTag = ID;
				return this.internalValues.get!(ID - 1)();
			}
			/++
			 + Checks the tag and returns the value of then union. Could throw an exception.
			 +
			 + Macros:
			 +     ID = Entry number to access. Counting from 1,2,…
			 +
			 + Returns: value of the entry
			 +/
			pure nothrow @nogc @live @trusted @property ref const(VALUES[ID - 1]) value(size_t ID)() const return if(
				1 <= ID && ID <= VALUES.length && !is(VALUES[ID - 1] == void)
			)
			{
				if(this.internalTag == ID)
				{
					return this.internalValues.get!(ID - 1);
				}
				else
				{
					assert(0);
				}
			}

			/++
			 + Move constructor.
			 +
			 + Params:
			 +     tuv = Source to move
			 +/
			pure nothrow @nogc @live @safe this(return ref TaggedUnionedValues!(VALUES) tuv)
			{
				this.internalTag = tuv.internalTag;
				slabel: switch(this.tag)
				{
					case 0:
						break;
					static foreach(T; TemplateTupleEnumerate!(VALUES))
					{
						case T.CONTENT[0] + 1:
							static if(!is(T.CONTENT[1] == void))
							{
								this.internalValues.set!(T.CONTENT[0])(tuv.internalValues.get!(T.CONTENT[0])());
							}
							break slabel;
					}
					default:
						assert(0);
				}
				tuv.internalTag = 0;
			}
			/++
			 + Deconstructor and deserialization.
			 +/
			pure nothrow @nogc @live @safe ~this()
			{
				this.internalDestroy();
			}

			/++
			 + Generats new tagged unioned values.
			 +
			 + Macros:
			 +     ID = Entry number to access. Counting from 1,2,…
			 +
			 + Returns: Generated result
			 +/
			static pure nothrow @nogc @live @trusted TaggedUnionedValues!(VALUES) gen(size_t ID)() return if(
				1 <= ID && ID <= VALUES.length && is(VALUES[ID - 1] == void)
			)
			{
				TaggedUnionedValues!(VALUES) result = {};
				result.internalType = ID;
				return result;
			}
			/++
			 + Generats new tagged unioned values.
			 +
			 + Macros:
			 +     ID = Entry number to access. Counting from 1,2,…
			 +
			 + Params:
			 +     value = Value to move.
			 +
			 + Returns: Generated result
			 +/
			static pure nothrow @nogc @live @trusted TaggedUnionedValues!(VALUES) gen(size_t ID)(return VALUES[ID - 1] value) if(
				1 <= ID && ID <= VALUES.length && !is(VALUES[ID - 1] == void)
			)
			{
				TaggedUnionedValues!(VALUES) result = {};
				result.internalTag = ID;
				result.internalValues.set!(ID - 1)(value);
				return result;
			}
		}
	}

	/++
	 + Generates an tagged union base on an union.
	 +
	 + Macros:
	 +     UNION = union to mimic
	 +/
	struct TaggedUnion(UNION)
	{
		static assert(is(UNION == union), "UNION has to be an union.");
		private
		{
			import std.traits : __Fields = Fields;

			TaggedUnionedValues!(__Fields!UNION) __ct = {};

			static pure @safe string __checkName()
			{
				import base.templatetuple : TemplateTupleEnumerate;
				import std.algorithm.searching : startsWith;
				import std.string : toLower;
				import std.traits : FieldNameTuple;

				foreach(string i; FieldNameTuple!(UNION))
				{
					switch(i.toLower())
					{
						case "__checkname":
							return "\"__checkName\" isn't allowed name.";
						case "__ct":
							return "\"__ct\" isn't allowed name.";
						case "__fields":
							return "\"__Fields\" isn't allowed name.";
						case "none":
							return "\"none\" isn't allowed name.";
						case "tag":
							return "\"tag\" isn't allowed name.";
						case "taggedunionedvalues":
							return "\"TaggedUnionedValues\" isn't allowed name.";
						case "tags":
							return "\"Tags\" isn't allowed name.";
						case "union":
							return "\"UNION\" isn't allowed name.";
						default:
							break;
					}
					if(i.toLower().startsWith("generate"))
					{
						return "Entry can't start with \"generate\".";
					}
				}
				return null;
			}
			static assert(__checkName() == null, __checkName());
		}
		public
		{
			// Generate Tags enum
			mixin(({
				import base.templatetuple : TemplateTupleEnumerate;
				import std.format : format;
				import std.string : join;
				import std.traits : FieldNameTuple;

				string result = void;
				if(FieldNameTuple!(UNION).length == 0)
				{
					result = "None";
				}
				else
				{
					string[FieldNameTuple!(UNION).length] tmp;
					static foreach(T; TemplateTupleEnumerate!(FieldNameTuple!(UNION)))
					{
						static if(T.CONTENT[1].length != 0 && 'a' <= T.CONTENT[1][0] && T.CONTENT[1][0] <= 'z')
						{
							tmp[T.CONTENT[0]] = format!("%c%s")(cast(char) (T.CONTENT[1][0] - 'a' + 'A'), T.CONTENT[1]);
						}
						else
						{
							tmp[T.CONTENT[0]] = T.CONTENT[1];
						}
					}
					result = "None, " ~ (cast(string[]) tmp).join(", ");
				}
				return format!"enum Tags {%s};"(result);
			})());
			/++
			 + Gives read access to the tag of the union.
			 +
			 + Returns: tag of the union
			 +/
			pure nothrow @nogc @live @safe @property Tags tag() const
			{
				import base.templatetuple : TemplateTupleEnumerate;
				import base.utils : staticFormat;
				import std.traits : FieldNameTuple;

				switch(this.__ct.tag)
				{
					case 0:
						return Tags.None;
					static foreach(T; TemplateTupleEnumerate!(FieldNameTuple!(UNION)))
					{
						case T.CONTENT[0] + 1:
							static if(T.CONTENT[1].length != 0 && 'a' <= T.CONTENT[1][0] && T.CONTENT[1][0] <= 'z')
							{
								return __traits(getMember, Tags, staticFormat!("%c%s", cast(char) (T.CONTENT[1][0] - 'a' + 'A'), T.CONTENT[1]));
							}
							else
							{
								return __traits(getMember, Tags, T.CONTENT[1]);
							}
					}
					default:
						assert(0);
				}
			}

			// Generate getters, setters and generators
			mixin({
				import base.templatetuple : TemplateTupleEnumerate;
				import std.format : format;
				import std.string : join;
				import std.traits : FieldNameTuple;

				const string temp = `
				%s
				pure nothrow @nogc @live @safe @property ref typeof(this.__ct.value!%s) %s()
				{
					return this.__ct.value!%s;
				}
				pure nothrow @nogc @live @safe @property ref const(typeof(this.__ct.value!%s)) %s() const
				{
					return this.__ct.value!%s;
				}
				pure nothrow @nogc @live @safe @property ref typeof(this.__ct.value!%s) %s(return typeof(this.__ct.value!%s) value)
				{
					return this.__ct.value!%s = value;
				}
				static pure nothrow @nogc @live @safe TaggedUnion!(UNION) generate%s(return __Fields!(UNION)[%s] value)
				{
					TaggedUnion!(UNION) result = {};
					result.__ct.value!%s = value;
					return result;
				}
				`;
				string result = "";
				static foreach(I; TemplateTupleEnumerate!(FieldNameTuple!UNION))
				{
					{
						string name;
						static if(I.CONTENT[1].length && 'a' <= I.CONTENT[1][0] && I.CONTENT[1][0] <= 'z')
						{
							name = format!"%c%s"(cast(char) (I.CONTENT[1][0] - 'a' + 'A'), I.CONTENT[1][1..$]);
						}
						else
						{
							name = I.CONTENT[1];
						}
						result = format!temp(
							result,
							I.CONTENT[0] + 1,
							I.CONTENT[1],
							I.CONTENT[0] + 1,
							I.CONTENT[0] + 1,
							I.CONTENT[1],
							I.CONTENT[0] + 1,
							I.CONTENT[0] + 1,
							I.CONTENT[1],
							I.CONTENT[0] + 1,
							I.CONTENT[0] + 1,
							name,
							I.CONTENT[0],
							I.CONTENT[0] + 1
						);
					}
				}
				return result;
			}());
		}
	}
}

version(unittest)
{
	private
	{
		import base.asserts : TesterDeconstructor, assertEquals, assertThrown, AssertError;

		// Test values
		unittest
		{
			{
				TesterDeconstructor.Notified n;
				{
					alias Type = TaggedUnionedValues!(TesterDeconstructor);
					Type tuv = Type.gen!1(TesterDeconstructor(&n));
				}
				n.check();
			}
			{
				TesterDeconstructor.Notified n1;
				TesterDeconstructor.Notified n2;
				{
					alias Type = TaggedUnionedValues!(TesterDeconstructor);
					Type tuv = Type.gen!1(TesterDeconstructor(&n1));
					tuv = Type.gen!1(TesterDeconstructor(&n2));
					Type tuv2;
					Type tuv3;
					assertEquals(tuv.isEmpty, false);
					assertEquals(tuv2.isEmpty, true);
					tuv2 = tuv3;
					assertEquals(tuv.isEmpty, false);
					assertEquals(tuv2.isEmpty, true);
					tuv2 = tuv;
					assertEquals(tuv.isEmpty, true);
					assertEquals(tuv2.isEmpty, false);
				}
				n1.check();
				n2.check();
			}
			{
				TesterDeconstructor.Notified n1, n2;
				{
					alias Type = TaggedUnionedValues!(TesterDeconstructor, TesterDeconstructor);
					Type tuv = Type.gen!1(TesterDeconstructor(&n1));
					assertEquals(tuv.tag, 1);
					tuv.value!2 = TesterDeconstructor(&n2);
					assertEquals(tuv.tag, 2);
					Type tuv2 = tuv;
				}
				n1.check();
				n2.check();
			}
			{
				TesterDeconstructor.Notified n1, n2, n3, n4;
				{
					alias Type = TaggedUnionedValues!(TesterDeconstructor, size_t);
					Type t1 = Type.gen!2(0xFF);
					t1.value!1 = TesterDeconstructor(&n1);
					Type t2 = Type.gen!1(TesterDeconstructor(&n2));
					t2.value!2 = 0xFF;
				}
				{
					alias Type = TaggedUnionedValues!(size_t, TesterDeconstructor);
					Type t1 = Type.gen!1(0xFF);
					t1.value!2 = TesterDeconstructor(&n3);
					Type t2 = Type.gen!2(TesterDeconstructor(&n4));
					t2.value!1 = 0xFF;
				}
				n1.check();
				n2.check();
				n3.check();
				n4.check();
			}
			{
				TaggedUnionedValues!(size_t, size_t) t;
				t.value!1 = 1;
				assertEquals(t.value!1, 1);
				const(TaggedUnionedValues!(size_t, size_t)) t2 = t;
				assertEquals(t2.value!1, 1);
			}
		}

		// Test tagged union
		unittest
		{
			{
				union Test
				{
					size_t ab;
					string cd;
				}
				TaggedUnion!Test tmp = TaggedUnion!Test.generateCd("test");
				assertThrown!(AssertError)(tmp.ab);
				assertEquals(tmp.cd, "test");
				tmp.ab = 10;
				assertEquals(tmp.ab, 10);
				assertThrown!(AssertError)(tmp.cd);
			}
			{
				union Test
				{
					TesterDeconstructor ab;
					string cd;
				}
				alias T = TaggedUnion!Test;
				TesterDeconstructor.Notified n;
				T tmp;
				tmp = T.generateAb(TesterDeconstructor(&n));
				tmp.cd = "test";
				assertEquals(tmp.cd, "test");
				n.check();
			}
		}
	}
}
