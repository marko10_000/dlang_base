# Base

Base utils for the programming language D. Supports:

- Better output for asserts and testers
- Safe unions with `TaggedUnionedValues` or `TaggedUnion`
- Add rust like type `Optinal` or `Result`

## Supported testers for debugging

- `TesterDeconstructor`: To test if container calls the deconstructor of a type correctly.
